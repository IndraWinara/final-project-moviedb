import { createUserWithEmailAndPassword, onAuthStateChanged, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { createContext, useCallback, useEffect, useState } from "react";
import { auth, db } from "../firebase/firebaseConfig";
import { doc, setDoc } from "firebase/firestore";
import { fetchCastMovies, fetchCastPerson, fetchDetailPerson, fetchDetailsMovies, fetchPopularMovies, fetchSearchMovie, fetchSimilarMovies, fetchTopRatedMovies, fetchTrendingMovies, fetchUpcomingMovies } from "../api/moviedb";
import { StackActions, useNavigation } from "@react-navigation/native";
import { debounce } from "lodash";



export const GlobalContext = createContext()

const GlobalProvider = ({children})=>{
const [user,setUser] = useState ({})
const [trending,setTrending] = useState ([])
const [upcoming,setUpcoming] = useState ([])
const [topRated,setTopRated] = useState ([])
const [isFavorite,setIsFavorite] = useState (false)
const [cast,setCast] = useState ([])
const [similar,setSimilar] = useState ([])
const [personMovies,setPersonMovies] = useState ([])
const [results,setResults] = useState ([])
const [loading,setLoading] = useState (false);
const [person,setPerson] = useState ([])
const [detailMovie,setDetailMovie] = useState ([])
const [popularMovie,setPopularMovie] = useState ([])
const [loadHider,setLoadHider] = useState (false)
const [userName,setUserName] = useState("")
const [genres,setGenres] = useState("")
const [favActor,setFavActor] = useState("")
const [imageUrl,setImageUrl] = useState("")
const [userInfo,setUserInfo] = useState("")
const [refresh,setRefresh] = useState(false)

// fungsi handleInput
const handleRefresh = ()=>{
    setRefresh(true)
    setTimeout(() => {
        setRefresh(false)
        setUserName("")
        setFavActor("")
        setGenres("")
        setImageUrl("")
    },1000);
}


const handleClick = (ress) => {
    navigation.dispatch(StackActions.push("Movie", ress));
  };

const handleHome = () => { navigation.navigate("screen")}

const handleReset = () => setResults([])

const handlePerson = (ress)=>{
    navigation.dispatch(StackActions.push("Person", ress))
}

const handleSeeAll = (ress) => {
    navigation.navigate("allmovies",ress );
  };

const handleBack = ()=> navigation.goBack()

const handleSearch = ()=> {
    navigation.navigate("Search")
}




const handleFindMovie = (ress)=>{
    if(ress && ress.length > 3) {
        setLoading(true)
        fetchSearchMovie({
            query: ress , 
            include_adult: 'false', 
            language: 'en-US',
            page: '1',
        })
        .then ((data)=> {
            setLoading(false)
            if (data && data.results) setResults(data.results)
        })
    }else {
        setResults([]) 
    };
}

const handleTextDebounce = useCallback(debounce(handleFindMovie,700),[])

// Fungsi Auth Firebase

const signUp = (email,password)=>{
    createUserWithEmailAndPassword(auth,email,password);
    setDoc(doc(db,"users",email),{
        user : [],
        data : []
    })
}

const logIn = (email,password)=>{
    return signInWithEmailAndPassword(auth,email,password)
}

const logOut = ()=>{
    return signOut(auth)
}

useEffect (()=>{
    const uncheck = onAuthStateChanged(auth,(currentUser)=>{
        setUser(currentUser)
    })
    return ()=>{
        uncheck()
    }
},[])

const fireInput= {
    signUp,
    logIn,
    logOut,
}

// useNavigation
const navigation = useNavigation()


// fetchData

const getTrendingMovies = async ()=>{
    const response = await fetchTrendingMovies()
    // console.log(response)
    if(response && response.results) setTrending(response.results)
    setLoading(true)
}
const getUpcomingMovies = async ()=>{
    const response = await fetchUpcomingMovies()
    // console.log(response)
    if(response && response.results) setUpcoming(response.results)
    setLoading(false)
}
const getTopRatedMovies = async ()=>{
    const response = await fetchTopRatedMovies()
    // console.log(response)
    if(response && response.results) setTopRated(response.results)
    setLoading(false)
}
const getPopularMovies = async () => {
    try {
      const response = await fetchPopularMovies();
      if (response && response.results) {
        return response.results
      }
    } catch (error) {
      // Tangani error dengan benar, misalnya menampilkan pesan error atau mencatat error tersebut.
      console.error('Error saat mengambil daftar film populer:', error);
    }
  };

  useEffect(() => {
    getPopularMovies().then((ress)=> setPopularMovie(ress))
  }, []);

const getCastPerson = async (id)=>{
    const response = await fetchCastPerson(id)
    // console.log(response)
    if(response && response.cast) setCast(response.cast)
    
}

const getDetailPerson = async (id)=>{
    const response = await fetchDetailPerson(id)
    // console.log("person",response)
    if(response && response) setPerson(response)
    
}
const getDetailsMovies = async (id)=>{
    const response = await fetchDetailsMovies(id)
    // console.log("Details by id",response)
    if(response && response) setDetailMovie(response)
    
}
const getSimilarMovies = async (id)=>{
    const response = await fetchSimilarMovies(id)
    // console.log("Similar by id",response.results)
    if(response && response.results) setSimilar(response.results)
    
}

const getCastMovies = async (id)=>{
    const response = await fetchCastMovies(id)
    // console.log("Similar by id",response.cast)
    if(response.cast && response.cast) setPersonMovies(response.cast)
    
}

// fetchInput
const fetchInput = {
    getTrendingMovies,
    getUpcomingMovies,
    getTopRatedMovies,
    getCastPerson,
    getDetailPerson,
    getDetailsMovies,
    getSimilarMovies,
    getCastMovies,
    getPopularMovies,
}

// state input
const state = {
    user,setUser,
    trending,setTrending,
    upcoming,setUpcoming,
    topRated,setTopRated,
    isFavorite,setIsFavorite,
    cast,setCast,
    similar,setSimilar,
    personMovies,setPersonMovies,
    results,setResults,
    loading,setLoading,
    person,setPerson,
    detailMovie,setDetailMovie,
    popularMovie,setPopularMovie,
    loadHider,setLoadHider,
    userName,setUserName,
    genres,setGenres,
    favActor,setFavActor,
    imageUrl,setImageUrl,
    userInfo,setUserInfo,
    refresh,setRefresh,
}

// handleInput
const handleInput = {
    handleClick,
    handleBack,
    handlePerson,
    handleSearch,
    handleHome,
    handleReset,
    handleSeeAll,
    handleTextDebounce,
    handleRefresh,
}

    return (
        <GlobalContext.Provider value={{
            state,
            handleInput,
            fireInput,
            fetchInput
        }}>
            {children}
        </GlobalContext.Provider>
    )
}

export default GlobalProvider