import { initializeApp } from "firebase/app";
import {initializeAuth,getReactNativePersistence} from "firebase/auth/react-native"
import { getFirestore } from "firebase/firestore";
import AsyncStorage from '@react-native-async-storage/async-storage';

const firebaseConfig = {
  apiKey: "AIzaSyAqgBnVgaATDVOmy1LOc1vsw4UPzd4cOG8",
  authDomain: "moviedb-react-native.firebaseapp.com",
  projectId: "moviedb-react-native",
  storageBucket: "moviedb-react-native.appspot.com",
  messagingSenderId: "551771439371",
  appId: "1:551771439371:web:7b4d16db50704cc811a20e"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = initializeAuth(app, {
  persistence: getReactNativePersistence(AsyncStorage)
});
export const db = getFirestore(app)

