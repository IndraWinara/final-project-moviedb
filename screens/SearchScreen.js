import {
  View,
  Text,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
} from "react-native";
import React, { useContext } from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { XMarkIcon } from "react-native-heroicons/outline";
import { GlobalContext } from "../context/GlobalContext";
import Loading from "../components/Loading";
import { dummyImage, image185 } from "../api/moviedb";
const { width, height } = Dimensions.get("window");
let picKosong = require("../assets/images/movieTime.png");

const SearchScreen = ({navigation}) => {
  const { handleInput, state } = useContext(GlobalContext);
  const {handleFindMovie,handleTextDebounce} = handleInput;
  const { results,loading,setResults } = state;
  const handleTo = (ress)=> {
    navigation.navigate("Movie",ress)
  }
  const handleReset = ()=>{
    setResults([])
  }
  return (
    <SafeAreaView className="bg-neutral-800 flex-1">
      <View className="mx-4 mb-3 flex-row justify-between items-center border border-neutral-500 rounded-full mt-5">
        <TextInput
          placeholder="Search Movie..."
          placeholderTextColor={"lightgray"}
          className="pb-1 pl-6 flex-1 text-base font-semibold text-white tracking-wider"
          onChangeText={handleTextDebounce}
        />
        <TouchableOpacity
          className="rounded-full p-3 m-1 bg-neutral-500"
          onPress={()=>handleReset()}
        >
          <XMarkIcon size="25" color="white" />
        </TouchableOpacity>
      </View>
      {/* results */}
      {loading ? (
        <Loading />
      ) : results?.length > 0 ? (
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 15 }}
          className="space-y-3"
        >
          <Text className="text-white font-semibold ml-2">
            Results ({results?.length})
          </Text>
          <View className="flex-row justify-between flex-wrap p-3">
            {results?.length > 0 && results?.map((ress, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => handleTo(ress) }
              >
                <View className="space-y-2 mb-4">
                  <Image
                    source={{uri : image185(ress?.poster_path) || dummyImage}}
                    className="rounded-3xl"
                    style={{ width: width * 0.44, height: height * 0.3 }}
                  />
                  <Text className="text-neutral-300 ml-1">
                    {ress?.title.length > 14
                      ? ress?.title.slice(0, 22) + "..."
                      : ress?.title}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      ) : ( 
        results ?
        <View className="flex-row justify-center">
          <Image source={picKosong} className="h-96 w-96" />
        </View> : <Loading/>
      )}
    </SafeAreaView>
  );
}

export default SearchScreen

