import { Image, StyleSheet, Text, TextInput, View } from "react-native";
import React, { useContext, useState } from "react";
import { TouchableOpacity } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { theme } from "../theme";
import { Dimensions } from "react-native";
import { Alert } from "react-native";
import { GlobalContext } from "../context/GlobalContext";

const { height, width } = Dimensions.get("window");

const LoginUser = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { fireInput } = useContext(GlobalContext);
  const { logIn } = fireInput;

  const handleLogin = async () => {
    const dataSignIn = {
      email: email,
      password: password,
    };
    for (const key in dataSignIn) {
      if (dataSignIn[key] === "") {
        Alert.alert("Mohon Lengkapi Data!!");
        return;
      }
      try {
        await logIn(email, password);
      } catch (error) {
        Alert.alert("id atau password salah");
      }
    }
  };
  return (
    <View
      className="flex-1 bg-white"
      style={{ backgroundColor: theme.background }}
    >
      <SafeAreaView className="flex">
        <View
          className="flex-row justify-center"
          style={{
            width: width,
            height: height * 0.29,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Image
            source={require("../assets/images/login.png")}
            style={{ width: 165, height: 110 }}
          />
        </View>
      </SafeAreaView>
      <View
        className="flex-1 bg-white px-8 pt-8"
        style={{ borderTopLeftRadius: 50, borderTopRightRadius: 50 }}
      >
        <View className="form space-y-2">
          <Text className="text-gray-700 ml-4">Email Address</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-3"
            placeholder="Enter Email"
            onChangeText={(value) => setEmail(value)}
          />
          <Text className="text-gray-700 ml-4">Password</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-7"
            secureTextEntry
            placeholder="Enter Password"
            onChangeText={(value) => setPassword(value)}
          />
          <TouchableOpacity
            className="py-3 bg-yellow-400 rounded-xl"
            onPress={() => handleLogin()}
          >
            <Text className="font-xl font-bold text-center text-gray-700">
              Login
            </Text>
          </TouchableOpacity>
        </View>

        <View className="flex-row justify-center mt-7">
          <Text className="text-gray-500 font-semibold">
            Dont have an account?
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate("signup")}>
            <Text className="font-semibold text-yellow-500"> SignUp</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default LoginUser;

const styles = StyleSheet.create({});
