import {
  View,
  Text,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import React, { useContext, useEffect } from "react";
import { useRoute } from "@react-navigation/native";
import { styles, theme } from "../theme";
import { GlobalContext } from "../context/GlobalContext";
import { ChevronLeftIcon } from "react-native-heroicons/outline";
import { HeartIcon } from "react-native-heroicons/solid";
import { SafeAreaView } from "react-native-safe-area-context";
import MovieList from "../components/MovieList";
import Loading from "../components/Loading";
import { dummyImage, image500 } from "../api/moviedb";
const { width, height } = Dimensions.get("window");

const PersonScreen = () => {
  const { params: ress } = useRoute();
  const { handleInput, state, fetchInput } = useContext(GlobalContext);
  const { handleBack} = handleInput;
  const { personMovies,person } = state;
  const { getDetailPerson, getCastMovies } = fetchInput;

  useEffect(() => {
    getDetailPerson(ress.id);
    getCastMovies(ress.id);
  }, [ress]);

  
  return (
    <ScrollView
      className="flex-1 bg-neutral-900"
      contentContainerStyle={{ paddingBottom: 20 }}
    >
      {/* back button */}
      <SafeAreaView className=" z-20 w-full flex-row justify-between items-center px-4 mt-4">
        <TouchableOpacity
          style={styles.background}
          className="rounded-xl p-1"
          onPress={handleBack}
        >
          <ChevronLeftIcon size="28" strokeWidth={2.5} color="white" />
        </TouchableOpacity>
        <TouchableOpacity >
          <HeartIcon
            size="35"
          />
        </TouchableOpacity>
      </SafeAreaView>

      {/* person details */}
      {person.length === 0 ? (
        <Loading />
      ) : (
        <View>
          <View
            className="flex-row justify-center my-5"
            style={{
              shadowColor: "gray",
              borderRadius: 160,
              shadowOpacity: 1,
              elevation: 13, // tambahkan properti elevation untuk Android
            }}
          >
            <View className="items-center jus rounded-full overflow-hidden h-72 w-72 border-2 border-neutral-700">
              <Image
                source={{ uri: image500(person?.profile_path) || dummyImage }}
                style={{ height: height * 0.51, width: width * 0.79 }}
              />
            </View>
          </View>
          {/* person title */}
          <View className="mt-6">
            <Text className="text-3xl text-white font-bold text-center">
              {person.name}
            </Text>
            <Text className="text-base text-neutral-500 text-center">
              {person?.place_of_birth}
            </Text>
          </View>
          {/* person info */}
          <View className="mx-3 p-4 mt-6 flex-row justify-between items-center bg-neutral-700 rounded-full">
            <View className="border-r-2 border-r-neutral-400 px-2 items-center">
              <Text className="text-white font-semibold">Gender</Text>
              <Text className="text-neutral-300 text-sm">
                {person?.gender === 2 ? "Male" : "Female"}
              </Text>
            </View>
            <View className="border-r-2 border-r-neutral-400 px-2 items-center">
              <Text className="text-white font-semibold">Birthday</Text>
              <Text className="text-neutral-300 text-sm">
                {person?.birthday}
              </Text>
            </View>
            <View className="border-r-2 border-r-neutral-400 px-2 items-center">
              <Text className="text-white font-semibold">Known For</Text>
              <Text className="text-neutral-300 text-sm">
                {person?.known_for_department}
              </Text>
            </View>
            <View className=" px-2 items-center">
              <Text className="text-white font-semibold">Popularity</Text>
              <Text className="text-neutral-300 text-sm">
                {person?.popularity}
              </Text>
            </View>
          </View>
          {/* person bio */}
          <View className="my-6 mx-4 space-y-2">
            <Text className="text-white text-lg">Biography</Text>
            <Text className="text-neutral-400 tracking-wide">
              {person?.biography}
            </Text>
          </View>
          {personMovies?.length === 0 ? (
            <Loading />
          ) : (
            <MovieList
              data={personMovies}
              hideSeeAll={false}
              title={"Movies"}
            />
          )}
        </View>
      )}
    </ScrollView>
  );
};

export default PersonScreen;

