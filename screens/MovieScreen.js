import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
} from "react-native";
import React, { useContext, useEffect, useState } from "react";
import { useRoute } from "@react-navigation/native";
import { SafeAreaView } from "react-native-safe-area-context";
import { ChevronLeftIcon } from "react-native-heroicons/outline";
import { HeartIcon } from "react-native-heroicons/solid";
import { styles, theme } from "../theme";
import { GlobalContext } from "../context/GlobalContext";
import { LinearGradient } from "expo-linear-gradient";
import Cast from "../components/Cast";
import MovieList from "../components/MovieList";
import Loading from "../components/Loading";
import { dummyImage, image500 } from "../api/moviedb";
import { arrayUnion, doc, updateDoc } from "firebase/firestore";
import { db } from "../firebase/firebaseConfig";
const { width, height } = Dimensions.get("window");

const MovieScreen = () => {
  const { params: ress } = useRoute();
  const { handleInput, state, fetchInput } = useContext(GlobalContext);
  const { handleBack} = handleInput;
  const {
    user,
    isFavorite,
    setIsFavorite,
    cast,
    similar,
    detailMovie,
  } = state;
  const { getCastPerson, getDetailsMovies, getSimilarMovies } = fetchInput;
  const movieDb = doc(db, 'users', `${user?.email}`);

  useEffect(() => {
    getCastPerson(ress.id);
    getSimilarMovies(ress.id);
    getDetailsMovies(ress.id);
  }, [ress]);


  const handleAddMovie = async () => {
    setIsFavorite(!isFavorite)
    await updateDoc(movieDb, {
      data : arrayUnion({
        id: ress.id,
        title: ress.title,
        image: ress.poster_path
      }),
    });
    setIsFavorite(false)
  };

  return (
    <ScrollView
      contentContainerStyle={{ paddingBottom: 20 }}
      className="flex-1 bg-neutral-900"
    >
      {/* back button and movie poster */}
      <View className="w-full">
        <SafeAreaView className=" absolute z-20 w-full flex-row justify-between items-center px-4 mt-4">
          <TouchableOpacity
            style={styles.background}
            className="rounded-xl p-1"
            onPress={handleBack}
          >
            <ChevronLeftIcon size="28" strokeWidth={2.5} color="white" />
          </TouchableOpacity>
          <TouchableOpacity onPress={handleAddMovie}>
            <HeartIcon
              size="35"
              color={isFavorite ? theme.background : "white"}
            />
          </TouchableOpacity>
        </SafeAreaView>
        {detailMovie.length === 0 ? (
          <Loading />
        ) : (
          <View>
            <Image
              source={{ uri: image500(detailMovie?.poster_path) || dummyImage }}
              style={{ width, height: height * 0.55 }}
            />
            <LinearGradient
              colors={["transparent", "rgba(23,23,23,0.8)", "rgba(23,23,23,1)"]}
              style={{ width, height: height * 0.4 }}
              start={{ x: 0.5, y: 0 }}
              end={{ x: 0.5, y: 1 }}
              className="absolute bottom-0"
            />
          </View>
        )}
      </View>
      {detailMovie.length === 0 ? (
        <Loading />
      ) : (
        <View style={{ marginTop: -(height * 0.09) }} className="space-y-3">
          {/* title */}
          <Text className="text-white text-center text-3xl font-bold tracking-wider">
            {detailMovie?.title}
          </Text>
          {/* status, release, runtime */}
          <Text className="text-neutral-400 font-semibold text-base text-center">
            {detailMovie?.status} •{" "}
            {detailMovie?.release_date &&
              detailMovie?.release_date.split("-")[0]}{" "}
            •{detailMovie?.runtime} min
          </Text>
          {/* genres */}
          <View className="flex-row justify-center mx-4 space-x-2">
            {detailMovie?.genres?.map((ress, index) => {
              let showDot = index + 1 != detailMovie?.genres.length;
              // console.log("genre:",ress)
              return (
                <Text
                  key={index}
                  className="text-neutral-400 font-semibold text-base text-center"
                >
                  {ress?.name} {showDot ? "•" : null}
                </Text>
              );
            })}
          </View>
          {/* description */}
          <Text className="text-neutral-400 mx-4 tracking-wide">
            {detailMovie?.overview}
          </Text>
        </View>
      )}
      {/* cast */}
      {cast?.length > 0 && cast?.length === 0 ? (
        <Loading />
      ) : (
        <Cast cast={cast} />
      )}

      {/* Similar Movie */}
      {similar?.length === 0 ? null : (
        <MovieList data={similar} title="Similar Movie" hideSeeAll={true} />
      )}
    </ScrollView>
  );
};

export default MovieScreen;
