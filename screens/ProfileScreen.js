import {
  Alert,
  Dimensions,
  Image,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useContext, useEffect } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { SafeAreaView } from "react-native-safe-area-context";
import { dummyImage, userDummy } from "../api/moviedb";
import { theme } from "../theme";
import {
  arrayUnion,
  doc,
  getDoc,
  onSnapshot,
  updateDoc,
} from "firebase/firestore";
import { db } from "../firebase/firebaseConfig";
import Loading from "../components/Loading";
const { width, height } = Dimensions.get("window");

const ProfileScreen = () => {
  const { state, handleInput } = useContext(GlobalContext);
  const {
    user,
    userName,
    setUserName,
    genres,
    setGenres,
    favActor,
    setFavActor,
    imageUrl,
    setImageUrl,
    userInfo,
    setUserInfo,
    refresh,
  } = state;
  const { handleRefresh } = handleInput;
  // console.log(user)
  const resetInputs = () => {
    setUserName("");
    setGenres("");
    setFavActor("");
    setImageUrl("");
  };

  useEffect(() => {
    const userDocRef = doc(db, "users", user?.email);
    const unsubscribe = onSnapshot(userDocRef, (docSnap) => {
      if (docSnap.exists()) {
        setUserInfo(docSnap.data()?.user || []);
      }
    });

    return () => unsubscribe();
  }, [user?.email]);

  // const movieDb = doc(db, "users", `${user?.email}`);

  const AddProfileName = async () => {
    if (userName === "") {
      Alert.alert("Lengkapi Dahulu");
      return;
    }

    const userDocRef = doc(db, "users", user?.email);
    const userDocSnap = await getDoc(userDocRef);
    if (userDocSnap.exists()) {
      const userData = userDocSnap.data();
      const updatedUserData = {
        ...userData,
        user: {
          ...userData.user,
          username: userName,
        },
      };
      await updateDoc(userDocRef, updatedUserData);
    }
  };

  const AddProfileGenres = async () => {
    if (genres === "") {
      Alert.alert("Lengkapi Dahulu");
      return;
    }

    const userDocRef = doc(db, "users", user?.email);
    const userDocSnap = await getDoc(userDocRef);
    if (userDocSnap.exists()) {
      const userData = userDocSnap.data();
      const updatedUserData = {
        ...userData,
        user: {
          ...userData.user,
          genres: genres,
        },
      };
      await updateDoc(userDocRef, updatedUserData);
    }
  };
  const AddProfileImageUrl = async () => {
    if (imageUrl === "") {
      Alert.alert("Lengkapi Dahulu");
      return;
    }

    const userDocRef = doc(db, "users", user?.email);
    const userDocSnap = await getDoc(userDocRef);
    if (userDocSnap.exists()) {
      const userData = userDocSnap.data();
      const updatedUserData = {
        ...userData,
        user: {
          ...userData.user,
          image_url: imageUrl,
        },
      };
      await updateDoc(userDocRef, updatedUserData);
    }
  };

  const AddProfileFavActor = async () => {
    if (favActor === "") {
      Alert.alert("Lengkapi Dahulu");
      return;
    }

    const userDocRef = doc(db, "users", user?.email);
    const userDocSnap = await getDoc(userDocRef);
    if (userDocSnap.exists()) {
      const userData = userDocSnap.data();
      const updatedUserData = {
        ...userData,
        user: {
          ...userData.user,
          actor: favActor,
        },
      };
      await updateDoc(userDocRef, updatedUserData);
    }
  };
  return (
    <View>
      <SafeAreaView
        className="flex justify-center items-center"
      >
        {userInfo ? (
          <View
            className="items-center justify-center rounded-full overflow-hidden border-2 border-neutral-700"
            style={{ width: width * 0.5, height: height * 0.25 }}
          >
            <Image
              source={{ uri: userInfo?.image_url || userDummy }}
              style={{
                height: height * 0.25,
                width: width * 0.5,
                objectFit: "cover",
              }}
            />
          </View>
        ) : (
          <Loading />
        )}
        <Text className="text-lg font-bold">
          {userInfo && userInfo?.username ? userInfo?.username : "username"}
        </Text>
        <Text className="text-sm">{user?.email}</Text>
      </SafeAreaView>
      <ScrollView
        style={{ height: height, padding: 10, marginVertical: 10 }}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refresh}
            onRefresh={() => handleRefresh()}
          />
        }
      >
        <Text style={{ marginBottom: 5 }}>Name</Text>
        <View className="flex flex-row justify-around items-center">
          <TextInput
            placeholder="Name..."
            value={userName}
            onChangeText={(value) => setUserName(value)}
            style={{
              padding: 10,
              borderWidth: 1,
              borderColor: theme.background,
              borderRadius: 8,
              marginLeft: 10,
              marginBottom: 5,
              width: width * 0.5,
            }}
          />
          <TouchableOpacity
            className="bg-red-500 p-3 rounded-xl"
            style={{ width: width * 0.2 }}
            onPress={() => AddProfileName()}
          >
            <Text className="text-center">Update</Text>
          </TouchableOpacity>
        </View>
        <Text style={{ marginBottom: 5 }}>Favorit Genre Movie</Text>
        <View className="flex flex-row justify-around items-center">
          <TextInput
            placeholder="Action Sci-fi"
            style={{
              padding: 10,
              borderWidth: 1,
              borderColor: theme.background,
              borderRadius: 8,
              marginLeft: 10,
              marginBottom: 5,
              width: width * 0.5,
            }}
            value={genres}
            onChangeText={(value) => setGenres(value)}
          />
          <TouchableOpacity
            className="bg-red-500 p-3 rounded-xl"
            style={{ width: width * 0.2 }}
            onPress={() => AddProfileGenres()}
          >
            <Text className="text-center">Update</Text>
          </TouchableOpacity>
        </View>
        <Text style={{ marginBottom: 5 }}>Favorit Actor/Actress</Text>
        <View className="flex flex-row justify-around items-center">
          <TextInput
            placeholder="Tom Cruise"
            style={{
              padding: 10,
              borderWidth: 1,
              borderColor: theme.background,
              borderRadius: 8,
              marginLeft: 10,
              marginBottom: 5,
              width: width * 0.5,
            }}
            value={favActor}
            onChangeText={(value) => setFavActor(value)}
          />
          <TouchableOpacity
            className="bg-red-500 p-3 rounded-xl"
            style={{ width: width * 0.2 }}
            onPress={() => AddProfileFavActor()}
          >
            <Text className="text-center">Update</Text>
          </TouchableOpacity>
        </View>
        <Text style={{ marginBottom: 5 }}>Image Url</Text>
        <View className="flex flex-row justify-around items-center">
          <TextInput
            placeholder="www.pic.com"
            style={{
              padding: 10,
              borderWidth: 1,
              borderColor: theme.background,
              borderRadius: 8,
              marginLeft: 10,
              marginBottom: 5,
              width: width * 0.5,
            }}
            value={imageUrl}
            onChangeText={(value) => setImageUrl(value)}
          />
          <TouchableOpacity
            className="bg-red-500 p-3 rounded-xl"
            style={{ width: width * 0.2 }}
            onPress={() => AddProfileImageUrl()}
          >
            <Text className="text-center">Update</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({});
