import { Image, StyleSheet, Text, View } from "react-native";
import React, { useContext, useState } from "react";
import { TouchableOpacity } from "react-native";
import { theme } from "../theme";
import { SafeAreaView } from "react-native-safe-area-context";
import { Dimensions } from "react-native";
import { TextInput } from "react-native";
import { Alert } from "react-native";
import { GlobalContext } from "../context/GlobalContext";
const { height, width } = Dimensions.get("window");

const SIgnUpUser = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { fireInput } = useContext(GlobalContext);
  const { signUp } = fireInput;

  const handleSignUp = async () => {
    const dataSignUp = {
      email: email,
      password: password,
    };
    for (const key in dataSignUp) {
      if (dataSignUp[key] === "") {
        Alert.alert("Mohon Lengkapi Data!!");
        return;
      }
    }
    try {
      await signUp(email, password);
    } catch (error) {
      console.log("signUp", error);
    }
  };

  return (
    <View
      className="flex-1 bg-white"
      style={{ backgroundColor: theme.background }}
    >
      <SafeAreaView className="flex">
        <View
          className="flex-row justify-center"
          style={{
            width: width,
            height: height * 0.23,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Image
            source={require("../assets/images/signup.png")}
            style={{ width: 165, height: 110 }}
          />
        </View>
      </SafeAreaView>
      <View
        className="flex-1 bg-white px-8 pt-8"
        style={{ borderTopLeftRadius: 50, borderTopRightRadius: 50 }}
      >
        <View className="form space-y-2">
          <Text className="text-gray-700 ml-4">Email Address</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-3 text-sm"
            placeholder="Enter Email"
            onChangeText={(value) => setEmail(value)}
          />
          <Text className="text-gray-700 ml-4">Password</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-7"
            secureTextEntry
            placeholder="Enter Password"
            onChangeText={(value) => setPassword(value)}
          />
          <TouchableOpacity
            className="py-3 bg-yellow-400 rounded-xl"
            onPress={() => handleSignUp()}
          >
            <Text className="font-xl font-bold text-center text-gray-700">
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>

        <View className="flex-row justify-center mt-7">
          <Text className="text-gray-500 font-semibold">
            Already have an account?
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate("login")}>
            <Text className="font-semibold text-yellow-500"> Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default SIgnUpUser;

const styles = StyleSheet.create({});
