import {
  Alert,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { onSnapshot, doc, updateDoc } from "firebase/firestore"; // Perbaiki impor doc dan getDoc
import { db } from "../firebase/firebaseConfig";
import { styles, theme } from "../theme";
import { dummyImage, image185 } from "../api/moviedb";
import { SafeAreaView } from "react-native-safe-area-context";
const { width, height } = Dimensions.get("window");

const UserScreen = ({ navigation }) => {
  const { fireInput, state } = useContext(GlobalContext);
  const { logOut } = fireInput;
  const { user } = state;
  const [userMovie, setUserMovie] = useState([]);

  useEffect(() => {
    const userDocRef = doc(db, "users", user?.email);
    const unsubscribe = onSnapshot(userDocRef, (docSnap) => {
      if (docSnap.exists()) {
        setUserMovie(docSnap.data()?.data || []);
      }
    });

    return () => unsubscribe();
  }, [user?.email]);

  // console.log(userMovie);

  const movieDb = doc(db, "users", user?.email);
  const handleDeleteMovies = async (id) => {
    try {
      const results = userMovie.filter((ress) => ress.id !== id);
      await updateDoc(movieDb, {
        data: results,
      });
    } catch (error) {
      console.log("delete movie user", error);
    }
  };
  const handleTo = (ress) => {
    navigation.navigate("Movie", ress);
  };

  return (
    <SafeAreaView
      className="flex justify-center items-center mt-[-20]">
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 15 }}
        className="space-y-3"
      >
        <View className="flex-row justify-between flex-wrap p-3">
          {userMovie.length > 0 ? (
            userMovie.map((ress, index) => (
              <View key={index} className="p-1">
                <TouchableOpacity
                  style={{
                    justifyContent: "flex-end",
                    alignItems: "flex-end",
                    opacity: 0.8,
                  }}
                  onPress={() => handleDeleteMovies(ress.id)}
                >
                  <Text
                    className="rounded-full"
                    style={{ backgroundColor: theme.background, padding: 3 }}
                  >
                    ❌
                  </Text>
                </TouchableOpacity>
                <TouchableWithoutFeedback
                  key={index}
                  onPress={() => handleTo(ress)}
                >
                  <View className="space-y-2 mb-4">
                    <Image
                      source={{ uri: image185(ress?.image) || dummyImage }}
                      className="rounded-3xl"
                      style={{ width: width * 0.44, height: height * 0.3 }}
                    />
                    <Text className="ml-1">
                      {ress?.title.length > 14
                        ? ress?.title.slice(0, 20) + "..."
                        : ress?.title}
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            ))
          ) : (
            <SafeAreaView style={{justifyContent:"center", alignItems:"center",height:height}}>
              <Text className="font-bold text-xl"> Find Your Favorite Movie in</Text>
              <Text className="text-lg">Movie <Text style={styles.text}>DB</Text> 😁</Text>
            </SafeAreaView>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default UserScreen;
