import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useContext, useEffect } from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { styles } from "../theme";
import { GlobalContext } from "../context/GlobalContext";
import TrendingFilm from "../components/TrendingFilm";
import MovieList from "../components/MovieList";
import Loading from "../components/Loading";
import { userDummy } from "../api/moviedb";
const { width, height } = Dimensions.get("window");

const HomeScreen = ({navigation}) => {
  const { state, handleInput, fetchInput } = useContext(GlobalContext);
  const { trending, upcoming, topRated, popularMovie, userInfo } = state;
  const { handleSearch } = handleInput;
  const { getTrendingMovies, getUpcomingMovies, getTopRatedMovies } =
    fetchInput;

  useEffect(() => {
    getTrendingMovies(), getUpcomingMovies(), getTopRatedMovies();
  }, []);

  return (
    <View className="flex-1 bg-neutral-800">
      {/* search bar and logo */}
      <SafeAreaView className="mb-3">
        {/* <StatusBar style="light" /> */}
        <View className="flex-row justify-between items-center mx-4 mt-3">
          {/* <Bars3CenterLeftIcon size={30} strokeWidth={2} color="white" /> */}
          <Text className="text-white text-3xl font-bold">
            <Text style={styles.text}>Movie</Text>DB™
          </Text>
          {userInfo ? <View className="flex justify-center items-center">
            <TouchableOpacity
              onPress={()=>navigation.navigate("profile")}
              className="items-center justify-center rounded-full overflow-hidden border-2 border-neutral-700"
              style={{ width: width * 0.15, height: height * 0.07 }}
            >
              <Image
                source={{ uri: userInfo?.image_url || userDummy }}
                style={{
                  height: height * 0.07,
                  width: width * 0.15,
                  objectFit: "cover",
                }}
              />
            </TouchableOpacity>
            <Text className="text-white">Welcome {userInfo.username ? userInfo.username : ""}</Text>
          </View> : null }
        </View>
      </SafeAreaView>
      {trending && upcoming && topRated.length === 0 ? (
        <Loading />
      ) : (
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 10 }}
        >
          <TrendingFilm data={trending} />
          {/* upcoming movies */}
          {upcoming.length > 0 && (
            <MovieList data={upcoming} title={"Upcoming"} />
          )}
          {/* topRated */}
          {topRated.length > 0 && (
            <MovieList data={topRated} title={"Top Rated"} />
          )}
          {/* Popular */}
          {popularMovie.length > 0 && (
            <MovieList data={popularMovie} title={"Popular"} />
          )}
        </ScrollView>
      )}
    </View>
  );
};

export default HomeScreen;
