import { Dimensions,ImageBackground,Text, TouchableOpacity, View } from "react-native";
import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { theme,styles } from "../theme";
const { width, height } = Dimensions.get("window");



const SplashScreen = ({navigation}) => {
  return (
    <ImageBackground
    source={require("../assets/images/splashscreen.png")}
    style={{
      flex : 1 ,
      resizeMode: 'cover',
      justifyContent: 'center',
      alignItems: 'center',
    }}
    >
      <SafeAreaView className="bg-neutral-700/60 flex justify-center items-center" style={{height:height*1.1, width:width}}>
      <View style={{justifyContent:"center",alignItems:"center", height:height*0.1}}>
        <Text className="text-4xl font-extrabold text-white"><Text style={styles.text} className="text-5xl">W</Text>elcome</Text>
      </View>
      <TouchableOpacity  onPress={()=>navigation.navigate("login")} className="border-4 border-yellow-400/50 flex justify-center items-center rounded-full" style={{height : height*0.238, width: width*0.5,backgroundColor: theme.background2, opacity:0.7}}>
        <Text className="text-4xl text-white font-bold">MovieD<Text style={styles.text}>B™</Text></Text>
      </TouchableOpacity>
      </SafeAreaView>
    </ImageBackground>
  );
};

export default SplashScreen;

