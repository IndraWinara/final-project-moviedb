// you can change these colors to change the look of the app ;)
export const theme = {
   background: '#eab308',
   text: '#eab308',
   background2: "#27272a",
}
export const styles = {
   text: {color: theme.text},
   background: {backgroundColor: theme.background}
}
