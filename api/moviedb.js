import axios from "axios";
import { apiKey,baseUrl,baseImageUrl } from "../constants";


// endpoint

const trendingMoviesEndPoint = `${baseUrl}/trending/movie/day?api_key=${apiKey}`
const upcomingMoviesEndPoint = `${baseUrl}/movie/upcoming?&page=1&api_key=${apiKey}`
const topRatedMoviesEndPoint = `${baseUrl}/movie/top_rated?&page=1&api_key=${apiKey}`
const popularMoviesEndPoint = `${baseUrl}/movie/popular?&page=1&api_key=${apiKey}`


// dynamic endpoint
const detailsMovies = (id) => `${baseUrl}/movie/${id}?api_key=${apiKey}`
const castPerson = (id)=> `${baseUrl}/movie/${id}/credits?api_key=${apiKey}`
const detailPerson = (id)=> `${baseUrl}/person/${id}?api_key=${apiKey}`
const similarEndPoint = (id)=> `${baseUrl}/movie/${id}/similar?api_key=${apiKey}`
const castMovieEndPoint = (id)=> `${baseUrl}/person/${id}/movie_credits?api_key=${apiKey}`
const searchMovieEndPoint = `${baseUrl}/search/movie?api_key=${apiKey}`

// image endpoint
export const image500 = (path)=> path ? `${baseImageUrl}/w500${path}`: null;
export const image342 = (path)=> path ? `${baseImageUrl}/w342${path}`: null;
export const image185 = (path)=> path ? `${baseImageUrl}/w185${path}`: null;

// dummy image
export const dummyImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6at7RwZOM_yVpsUZWimO0o75bYbKAE1DaTg&usqp=CAU"
export const userDummy = "https://pbs.twimg.com/profile_images/1321030814436655106/87OcbZNm_400x400.jpg"


const apiCall = async (endpoint,params) => {
    const options = {
        method : "GET",
        url : endpoint,
        params : params ? params : {}
    }

    try {
        const response = await axios.request (options)
        return response.data
    }
    catch (error) {
        console.log(error);
        return {}
    }
}

export const  fetchTrendingMovies = () => {
   return apiCall (trendingMoviesEndPoint)
}

export const  fetchUpcomingMovies = () => {
   return apiCall (upcomingMoviesEndPoint)
}

export const  fetchTopRatedMovies = () => {
   return apiCall (topRatedMoviesEndPoint)
}
export const  fetchPopularMovies = () => {
   return apiCall (popularMoviesEndPoint)
}

export const  fetchCastPerson = (id) => {
    return apiCall (castPerson(id))
 }

export const  fetchDetailPerson = (id) => {
    return apiCall (detailPerson(id))
 }

export const  fetchDetailsMovies = (id) => {
    return apiCall (detailsMovies(id))
 }
 
export const  fetchSimilarMovies = (id) => {
    return apiCall (similarEndPoint(id))
 }

export const  fetchCastMovies = (id) => {
    return apiCall (castMovieEndPoint(id))
 }

export const fetchSearchMovie = (params) =>{
    return apiCall(searchMovieEndPoint,params)
}
