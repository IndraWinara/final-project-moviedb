import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import Navigator from "./Navigator";
import GlobalProvider from "../context/GlobalContext";

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <GlobalProvider>
        <Navigator />
      </GlobalProvider>
    </NavigationContainer>
  );
};

export default AppNavigator;
