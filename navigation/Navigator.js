import React, { useContext } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import SplashScreen from "../screens/SplashScreen";
import LoginUser from "../screens/LoginUser";
import SIgnUpUser from "../screens/SIgnUpUser";
import HomeScreen from "../screens/HomeScreen";
import SearchScreen from "../screens/SearchScreen";
import UserScreen from "../screens/UserScreen";
import { GlobalContext } from "../context/GlobalContext";
import AllMovies from "../components/AllMovies";
import PersonScreen from "../screens/PersonScreen";
import MovieScreen from "../screens/MovieScreen";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { theme } from "../theme";
import Ionicons from "react-native-vector-icons/Ionicons";
import {
  DrawerContentScrollView,
  DrawerItemList,
  createDrawerNavigator,
} from "@react-navigation/drawer";
import ProfileScreen from "../screens/ProfileScreen";
import { Alert, Dimensions, Text, TouchableOpacity, View } from "react-native";

const { width, height } = Dimensions.get("window");
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const Draw = createDrawerNavigator();
const Navigator = () => {
  const { state, fireInput } = useContext(GlobalContext);
  const { user } = state;
  // console.log(user?.email)
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {!user?.email ? (
        <>
          <Stack.Screen name="splash" component={SplashScreen} />
          <Stack.Screen name="login" component={LoginUser} />
          <Stack.Screen name="signup" component={SIgnUpUser} />
        </>
      ) : (
        <Stack.Screen name="home" component={MyTabs} />
      )}
    </Stack.Navigator>
  );
};

export default Navigator;

const MyTabs = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size }) => {
          let iconName;

          if (route.name === "main") {
            iconName = "home";
          } else if (route.name === "user") {
            iconName = "person";
          } else if (route.name === "search") {
            iconName = "search";
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        headerShown: false,
        tabBarActiveTintColor: theme.background2,
        tabBarActiveBackgroundColor: theme.background,
        tabBarInactiveBackgroundColor: theme.background,
      })}
    >
      <Tab.Screen name="main" component={MyStacks} />
      <Tab.Screen name="search" component={SearchScreen} />
      <Tab.Screen name="user" component={MyDraw} />
    </Tab.Navigator>
  );
};

const MyStacks = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen
        name="screen"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Movie"
        options={{ headerShown: false }}
        component={MovieScreen}
      />
      <Stack.Screen
        name="Person"
        options={{ headerShown: false }}
        component={PersonScreen}
      />
      <Stack.Screen
        name="Search"
        options={{ headerShown: false }}
        component={SearchScreen}
      />
      <Stack.Screen
        name="allmovies"
        options={{ headerShown: false }}
        component={AllMovies}
      />
    </Stack.Navigator>
  );
};

const MyDraw = () => {
  return (
    <Draw.Navigator
      screenOptions={{
        drawerActiveBackgroundColor: theme.background,
        drawerInactiveTintColor: theme.background,
        drawerActiveTintColor: "white",
        headerStyle: {
          backgroundColor: theme.background,
          borderBottomColor: "#171717",
          borderBottomWidth: 1,
        },
        headerTitleAlign: "center",
        drawerStyle: {
          backgroundColor: "#171717",
          width: width * 0.55,
          height:height*0.7,
          opacity: 0.9,
          borderBottomRightRadius: 20,
          borderTopRightRadius: 20,
          marginTop:height*0.2
        },
        drawerContentStyle: { marginTop: height * 0.05 },
        
      }}
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      <Draw.Screen
        name="watch"
        component={UserScreen}
        options={{
          title: "Watch List",
        }}
      />
      <Draw.Screen
        name="profile"
        component={ProfileScreen}
        options={{ title: "Account" }}
      />
    </Draw.Navigator>
  );
};

function CustomDrawerContent(props) {
  const { fireInput } = useContext(GlobalContext);
  const { logOut } = fireInput;

  const handleLogOut = async () => {
    try {
      await logOut().then(() => {
        Alert.alert("Berhasil LogOut");
      });
    } catch (error) {
      console.log("logOut", error);
    }
  };

  return (
    <DrawerContentScrollView>
      {/* Konten menu navigasi samping */}
      <DrawerItemList {...props} />
      {/* Tambahkan tombol log out */}
      <View style={{height:height*0.45, justifyContent:"flex-end", marginBottom:30}}>
        <TouchableOpacity
          onPress={handleLogOut}
          style={{ marginHorizontal: 16, marginTop: 16,backgroundColor: "red", padding:10, borderRadius: 8 }}
        >
          <Text style={{ color: "white", fontSize: 16,textAlign:"center" }}>Log Out</Text>
        </TouchableOpacity>
      </View>
    </DrawerContentScrollView>
  );
}
