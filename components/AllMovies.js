import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import React, { useContext } from "react";
import { useEffect } from "react";
import { useRoute } from "@react-navigation/native";
import Loading from "./Loading";
import { GlobalContext } from "../context/GlobalContext";
import { SafeAreaView } from "react-native-safe-area-context";
import { dummyImage, image185 } from "../api/moviedb";
const { width, height } = Dimensions.get("window");

const AllMovies = () => {
  const { params: ress } = useRoute();
  const {handleInput } = useContext(GlobalContext);
  const { handleClick } = handleInput;
  
  useEffect(() => {

  }, [ress]);

  if (!ress) {
    return <Loading />;
  }



  return (
    <SafeAreaView
      className="flex justify-center items-center bg-neutral-800 "
      style={{ height: height}}
    >
      <View className="flex justify-center items-center  h-20">
        <Text className="text-2xl text-white font-bold">{ress.title}</Text>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 15 }}
        className="space-y-3"
      >
        <View className="flex-row justify-between flex-wrap p-3">
          {ress.data.map((ress, index) => (
            <TouchableWithoutFeedback
              key={index}
              onPress={() => handleClick(ress)}
            >
              <View className="space-y-2 mb-4">
                <Image
                  source={{ uri: image185(ress?.poster_path) || dummyImage }}
                  className="rounded-3xl"
                  style={{ width: width * 0.44, height: height * 0.3 }}
                />
                <Text className="text-neutral-300 ml-1">
                  {ress?.title.length > 14
                    ? ress?.title.slice(0, 20) + "..."
                    : ress?.title}
                </Text>
              </View>
            </TouchableWithoutFeedback>
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AllMovies;

const styles = StyleSheet.create({});
