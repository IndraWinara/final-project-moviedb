import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native'
import React, { useContext } from 'react'
import { GlobalContext } from '../context/GlobalContext'
import {image185, userDummy } from '../api/moviedb'

const Cast = ({cast}) => {
    const {handleInput} = useContext(GlobalContext)
    const {handlePerson}= handleInput

  return (
    <View className="my-6">
      <Text className="text-white text-lg mx-4 mb-5">Top Cast</Text>
      <ScrollView
      horizontal
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{paddingBottom:15}}
      >
        {cast && cast?.slice(0,9).map((ress,index)=>(
            <TouchableOpacity
            key={index}
            className="mr-4 items-center"
            onPress={()=>handlePerson(ress)}
            >
                <View className="overflow-hidden rounded-full h-20 w-20 border border-neutral-500 items-center">

                <Image className="w-20 h-24 rounded-2xl" source={{uri : image185(ress?.profile_path) || userDummy}}/>
                </View>
                <Text className="text-white text-xs mt-1">
                    {ress?.character.length> 10 ? ress?.character.slice(0,10)+ "..." : ress?.character }
                </Text>
                <Text className="text-white text-xs mt-1">
                    {ress?.original_name.length>10 ? ress?.original_name.slice(0,10)+ "..." : ress?.original_name }
                </Text>
            </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  )
}

export default Cast