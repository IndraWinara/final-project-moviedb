import {
  View,
  Text,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
} from "react-native";
import React, { useContext } from "react";
import Carousel from "react-native-snap-carousel";
const { width, height } = Dimensions.get("window");
import { GlobalContext } from "../context/GlobalContext";
import { image500 } from "../api/moviedb";

const TrendingFilm = ({ data }) => {
  return (
    <View>
      <Text className="text-white text-xl mx-4 mb-5">Trending</Text>
      <Carousel
        data={data}
        renderItem={({ item }) => <MovieCard item={item} />}
        firstItem={1}
        inactiveSlideOpacity={0.6}
        sliderWidth={width}
        itemWidth={width * 0.62}
        slideStyle={{ display: "flex", alignItems: "center" }}
      />
    </View>
  );
};

export default TrendingFilm;

const MovieCard = ({ item }) => {
  const { handleInput } = useContext(GlobalContext);
  const { handleClick } = handleInput;
  return (
    <TouchableWithoutFeedback onPress={() => handleClick(item)}>
      <Image
        source={{ uri: image500(item.poster_path) }}
        style={{ width: width * 0.6, height: height * 0.4 }}
        className="rounded-xl"
      />
    </TouchableWithoutFeedback>
  );
};
