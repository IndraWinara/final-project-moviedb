import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
} from "react-native";
import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { styles } from "../theme";
import { dummyImage, image185 } from "../api/moviedb";
import Loading from "./Loading";
const { width, height } = Dimensions.get("window");

const MovieList = ({ data, title, hideSeeAll }) => {
  const { handleInput } = useContext(GlobalContext);
  const { handleClick, handleSeeAll } = handleInput;

  return (
    <View className="mb-8 space-y-4 mt-2">
      <View className="mx-4 flex-row justify-between items-center">
        <Text className="text-white text-xl">{title}</Text>

        {!hideSeeAll && (
          <TouchableOpacity
            onPress={() =>
              handleSeeAll({
                title: title,
                data: data,
              })
            }
          >
            <Text style={styles.text} className="text-lg">
              See All
            </Text>
          </TouchableOpacity>
        )}
      </View>
      {/* movie scroll */}
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ padding: 15 }}
      >
        {data.length === 0 ? (
          <Loading />
        ) : (
          data &&
          data?.map((ress, index) => (
            <TouchableWithoutFeedback
              key={index}
              onPress={() => handleClick(ress)}
            >
              <View className="space-y-1 mr-4">
                <Image
                  source={{ uri: image185(ress?.poster_path) || dummyImage }}
                  className="rounded-3xl"
                  style={{ width: width * 0.33, height: height * 0.22 }}
                />
                <Text className="text-neutral-300 ml-1">
                  {ress.title && ress?.title.length > 14
                    ? ress?.title.slice(0, 14) + "..."
                    : ress?.title}
                </Text>
              </View>
            </TouchableWithoutFeedback>
          ))
        )}
      </ScrollView>
    </View>
  );
};

export default MovieList;
